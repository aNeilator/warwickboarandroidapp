WarwickBoarAndroidApp
=====================

An Android Client for the Warwick Student newspaper The Boar.

The Boar (www.theboar.org) is the official student newspaper at the University of Warwick.
This mobile app attempts to provide a version of the website, optimised for phones and tablets.


References
==========

Sliding Menu Library. You must reference this library in order to compile.
https://github.com/jfeinstein10/SlidingMenu.git
